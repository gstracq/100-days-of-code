def main():
    print(f'Welcome to the tip calculator.')

    bill = float(input(f'What was the total bill? $'))
    tip = int(input(f'What percentage tip would you like to give? 10, 12 or 15? '))

    if tip not in [10, 12, 15]:
        print(f'This tip is not valid!')
        print(f'Please, run this program again.')
        return

    nr_people = int(input(f'How many people to split the bill? '))

    total_bill = bill * (1 + tip/100)
    total_per_person = total_bill/nr_people

    print(f'Total bill: ${round(total_bill, 2):.2f}')
    print(f'Each person should pay: ${round(total_per_person, 2):.2f}')


if __name__ == '__main__':
    main()
