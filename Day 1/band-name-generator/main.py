def main():
    print(f'Welcome to the Band Name Generator.')
    city_name = input(f'What\'s name of the city you grew up in?\n')
    pet_name = input(f'What\'s the name of a pet?\n')

    return print(f'Your band name could be {city_name} {pet_name}')


if __name__ == '__main__':
    main()
