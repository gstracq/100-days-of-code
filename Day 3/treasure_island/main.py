def ascii_art():
    print('''
 *******************************************************************************
          |                   |                  |                     |
 _________|________________.=""_;=.______________|_____________________|_______
|                   |  ,-"_,=""     `"=.|                  |
|___________________|__"=._o`"-._        `"=.______________|___________________
          |                `"=._o`"=._      _`"=._                     |
 _________|_____________________:=._o "=._."_.-="'"=.__________________|_______
|                   |    __.--" , ; `"=._o." ,-"""-._ ".   |
|___________________|_._"  ,. .` ` `` ,  `"-._"-._   ". '__|___________________
          |           |o`"=._` , "` `; .". ,  "-._"-._; ;              |
 _________|___________| ;`-.o`"=._; ." ` '`."\` . "-._ /_______________|_______
|                   | |o;    `"-.o`"=._``  '` " ,__.--o;   |
|___________________|_| ;     (#) `-.o `"=.`_.--"_o.-; ;___|___________________
____/______/______/___|o;._    "      `".o|o_.--"    ;o;____/______/______/____
/______/______/______/_"=._o--._        ; | ;        ; ;/______/______/______/_
____/______/______/______/__"=._o--._   ;o|o;     _._;o;____/______/______/____
/______/______/______/______/____"=._o._; | ;_.--"o.--"_/______/______/______/_
____/______/______/______/______/_____"=.o|o_.--""___/______/______/______/____
/______/______/______/______/______/______/______/______/______/______/[TomekK]
*******************************************************************************
        ''')


def main():
    print(f'Welcome to Treasure Island!\n')
    print(f'Your mission is to find the treasure.')

    step1 = input(f'You\'re at a cross road. Where do you want to go? Type "left" or "right": ').lower()

    if step1 == 'right':
        print(f'You fell into a hole and died.')
        return

    step2 = input(f'You\'ve come to a lake. There is an island in the middle of the lake. Type "swim" to swim across '
                  f'or "wait" to wait for a boat: ').lower()

    if step2 == 'swim':
        print(f'You got attacked by an angry trout and died.')
        return

    step3 = input(f'You arrive at the island unharmed. There is a house with 3 doors. One red, one yellow and one blue.'
                  f' Which colour do you choose? ').lower()

    if step3 == 'yellow':
        print(f'You found the treasure! You Win!')
    elif step3 == 'blue':
        print(f'You entered in a room full of beasts. You died.')
    elif step3 == 'red':
        print(f'It\'s a room full of fire! You died.')

    return


if __name__ == "__main__":
    ascii_art()
    main()
    print(f'Game Over')
